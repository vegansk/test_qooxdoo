qx.Class.define('instr_to_pledge.FesPledgeConditionPanel', (function() {
    
    var Settings = instr_to_pledge.Settings,
        Button = qx.ui.form.Button,
        Label = qx.ui.basic.Label,
        TextField = qx.ui.form.TextField,
        DateField = qx.ui.form.DateField,
        CheckBox = qx.ui.form.CheckBox,
        SelectBox = qx.ui.form.SelectBox,
        Utils = instr_to_pledge.Utils,
        ListItem = qx.ui.form.ListItem,
        TextArea = qx.ui.form.TextArea;
    
    return {
        extend: qx.ui.container.Composite,
        
        construct: function() {
            this.base(arguments);
            
            this.__initComponents();
        },
        
        members: {
            __initComponents: function() {
                this.setLayout(new qx.ui.layout.Grid(Settings.MIN_GAP, Settings.MIN_GAP));
                this.getLayout().setColumnFlex(9, 1);
                this.getLayout().setColumnAlign(0, 'right', 'middle');
                this.getLayout().setColumnAlign(1, 'left', 'middle');
                this.getLayout().setRowAlign(5, 'right', 'middle');
                this.getLayout().setRowAlign(6, 'right', 'middle');
                this.getLayout().setRowAlign(7, 'right', 'middle');
                this.getLayout().setRowAlign(8, 'right', 'middle');
                this.getLayout().setRowAlign(12, 'right', 'middle');
                this.getLayout().setColumnWidth(4, 160);
                this.getLayout().setColumnWidth(6, 150);
                
                this.add(new CheckBox(), {row: 0, column: 0});
                this.add(new Label('Последующий залог запрещается'), {row: 0, column: 1, colSpan: 9});
                
                this.add(new CheckBox(), {row: 1, column: 0});
                this.add(new Label('Передача заложенных ЦБ допускается без согласия залогодержателя'), {row: 1, column: 1, colSpan: 9});
                
                this.add(new CheckBox(), {row: 2, column: 0});
                this.add(new Label('Уступка прав по договору залога ЦБ без согласия залогодателя запрещается'), {row: 2, column: 1, colSpan: 9});
                
                this.add(new CheckBox(), {row: 3, column: 0});
                this.add(new Label('Залог распространяется на все количество ЦБ после конвертации'), {row: 3, column: 1, colSpan: 9});
                
                this.add(new CheckBox(), {row: 4, column: 0});
                this.add(new Label('Залог распространяется на дополнительно зачисляемые на счет залогодателя ЦБ'), {row: 4, column: 1, colSpan: 9});
                
                this.add(new Label('Вид:'), {row: 5, column: 1});
                
                this.secAddTypeSelect = new SelectBox();
                this.__setupSecurityType(this.secAddTypeSelect);
                this.add(this.secAddTypeSelect, {row: 5, column: 2});
                
                this.add(new Label('Категория:'), {row: 5, column: 3});
                
                this.secAddKindSelect = new SelectBox();
                this.__setupSecurityKind(this.secAddKindSelect);
                this.add(this.secAddKindSelect, {row: 5, column: 4});
                
                this.secPartCheck = new CheckBox('На часть ЦБ:');
                this.add(this.secPartCheck, {row: 5, column: 6});
                
                this.secPartEdit = new TextField();
                this.add(this.secPartEdit, {row: 5, column: 7});
                
                this.add(new Label('шт.'), {row: 5, column: 8});

                this.add(new CheckBox(), {row: 6, column: 0});
                this.add(new Label('Получателем дивидендов является залогодержатель'), {row: 6, column: 1, colSpan: 5});
                
                this.dividPlegdeePartSelect = new SelectBox();
                this.__setupPartSelect(this.dividPlegdeePartSelect);
                this.add(this.dividPlegdeePartSelect, {row: 6, column: 6});
                
                this.secPartEdit = new TextField();
                this.add(this.secPartEdit, {row: 6, column: 7});
                
                this.add(new Label('шт.'), {row: 6, column: 8});
                
                this.add(new CheckBox(), {row: 7, column: 0});
                this.add(new Label('Правом голоса обладает залогодержатель'), {row: 7, column: 1, colSpan: 5});
                
                this.votesPlegdeePartSelect = new SelectBox();
                this.__setupPartSelect(this.votesPlegdeePartSelect);
                this.add(this.votesPlegdeePartSelect, {row: 7, column: 6});
                
                this.votesPartEdit = new TextField();
                this.add(this.votesPartEdit, {row: 7, column: 7});
                
                this.add(new Label('шт.'), {row: 7, column: 8});
                
                this.add(new CheckBox(), {row: 8, column: 0});
                this.add(new Label('Обращение взыскания на заложенные ЦБ осуществляется во внесудебном порядке'), {row: 8, column: 1, colSpan: 9});
                
                var label = new Label('Дата обращения взыскания:');
                label.setAllowGrowX(true);
                this.add(label, {row: 9, column: 1, colSpan: 3});
                
                var panel = new qx.ui.container.Composite();
                panel.setLayout(new qx.ui.layout.Grid(Settings.MIN_GAP, Settings.MIN_GAP));
                panel.getLayout().setColumnFlex(1, 1);
                panel.getLayout().setRowAlign(0, 'left', 'middle');
                
                var field = new DateField();
                panel.add(field, {row: 0, column: 0});
                label = new Label('Срок реализации заложенных ЦБ:');
                label.setMarginLeft(10);
                panel.add(label, {row: 0, column: 1});
                
                this.add(panel, {row: 9, column: 4, colSpan: 3});
                
                this.add(new DateField(), {row: 9, column: 7});
                
                this.add(new Label('Документы, предоставляемые залогодержателем реестродержателю'), {row: 10, column: 1, colSpan: 7});
                
                var list = this.__createList();
                this.add(list, {row: 11, column:1 , colSpan: 9});
                
                this.add(new CheckBox(), {row: 12, column: 0});
                this.add(new Label('Залог в обеспечение исполнения обязательств по облигациям'), {row: 12, column: 1, colSpan: 9});
                
                label = new Label('Дополнительные условия');
                label.setAllowGrowX(false);
                label.setAlignX('left');
                this.add(label, {row: 13, column: 0, colSpan: 3});
                
                this.addCond = new TextArea();
                this.addCond.setMinimalLineHeight(2);
                this.add(this.addCond, {row: 14, column: 0, colSpan: 10});
                
                panel = new qx.ui.container.Composite();
                panel.setMarginTop(Settings.GAP * 2);
                panel.setLayout(new qx.ui.layout.Grid(Settings.MIN_GAP, Settings.MIN_GAP));
                panel.getLayout().setColumnFlex(1, 1);
                
                panel.add(new Button('...'), {row: 0, column: 0});
                
                var button = new Button('ОК');
                button.setAllowGrowX(false);
                button.setAlignX('right');
                panel.add(button, {row: 0, column: 1});
                
                panel.add(new Button('Отмена'), {row: 0, column: 2});
                
                this.add(panel, {row: 15, column: 0, colSpan: 10});
            },
            
            __setupSecurityType: function(el) {
                el.add(new ListItem('Акция'));
                el.add(new ListItem('Облигация'));
            },
            
            __setupSecurityKind: function(el) {
                el.add(new ListItem('<Не указан>'));
                el.add(new ListItem('Обыкновенная'));
                el.add(new ListItem('Привелегированная'));
            },
            
            __setupPartSelect: function(el) {
                el.add(new ListItem('На все количество'));
                el.add(new ListItem('На часть ЦБ'));
            },
            
            __createList: function() {
                
                var items = [
                    'Выписка из реестра сделок организатора торгов, подтверждающая заключение сделки',
                    'Договор купли-продажи ЦБ, заключенный комиссионером, и договор комиссии между залогодержателем и комиссионером',
                    'Протокол несостоявшихся повторных торгов',
                    'Выписка из реестра сделок организатора торгов, подтверждающая заключение сделки 2',
                    'Договор купли-продажи ЦБ, заключенный комиссионером, и договор комиссии между залогодержателем и комиссионером 2',
                    'Протокол несостоявшихся повторных торгов 2'
                ];

                var list = new qx.ui.form.List();
                var controller = new qx.data.controller.List(null, list);
                var data = new qx.data.Array(items);
                
                controller.setModel(data);
                
                var delegate = {
                    configureItem: function(item) {
                        item.setMargin(Settings.MIN_GAP);
                        item.setPadding(0);
                    },
                    createItem: function() {
                        return new qx.ui.form.CheckBox();
                    }
                };
                
                controller.setDelegate(delegate);
                
                list.setHeight(70);
                list.setSelectionMode('single');
                
                return list;
            }
        }
    };
    
})());