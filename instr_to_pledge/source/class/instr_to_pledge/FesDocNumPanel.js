qx.Class.define('instr_to_pledge.FesDocNumPanel', (function() {

    // Imports
    var Settings = instr_to_pledge.Settings,
        Label = qx.ui.basic.Label,
        TextField = qx.ui.form.TextField,
        DateField = qx.ui.form.DateField, 
        Button = qx.ui.form.Button;


    return {
        extend: qx.ui.container.Composite,

        construct: function() {
            this.base(arguments);

            this.initComponents();
        },

        members: {
            initComponents: function() {
                var layout = new qx.ui.layout.Grid(Settings.GAP, Settings.GAP);

                layout.setColumnAlign(0, "right", "middle");
                layout.setColumnFlex(3, 1);
                layout.setColumnAlign(3, "right", "middle");

                this.setLayout(layout);

                this.numDateLabel = new Label('№, дата документа:');

                this.add(this.numDateLabel, {
                    row: 0,
                    column: 0
                });
                
                this.numEdit = new TextField();
                this.add(this.numEdit, {row: 0, column: 1});
                
                this.dateEdit = new DateField();
                this.add(this.dateEdit, {row: 0, column: 2});
                
                this.taNumDateLabel = new Label('№, дата документа у ТА:');
                this.add(this.taNumDateLabel, {row: 0, column: 3});
                
                this.taNumEdit = new TextField();
                this.add(this.taNumEdit, {row: 0, column: 4});
                
                this.taDateEdit = new DateField();
                this.add(this.taDateEdit, {row: 0, column: 5});
                
                this.senderLabel = new Label('Отправитель:');
                this.add(this.senderLabel, {
                    row: 1,
                    column: 0
                });
                
                {
                    this.senderPanel = new qx.ui.container.Composite();
                    this.senderPanel.setLayout(new qx.ui.layout.Grid());
                    this.senderPanel.getLayout().setColumnFlex(0, 1);
                    this.senderPanel.getLayout().setRowAlign(0, 'center', 'middle');
                    this.add(this.senderPanel, {row: 1, column: 1, colSpan: 5});
                    
                    this.senderEdit = new TextField();
                    this.senderEdit.setMarginRight(Settings.GAP);
                    this.senderPanel.add(this.senderEdit, {row: 0, column: 0});
                    
                    this.senderButton = new Button("...");
                    this.senderPanel.add(this.senderButton, {row: 0, column: 1});
                }
            },

            getLabelSize: function() {
                return this.numDateLabel.getBounds().width;
            },

            setLabelSize: function(size) {
                this.getLayout().setColumnWidth(0, size);
            }
        }

    };

})());