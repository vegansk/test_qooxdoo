qx.Class.define('instr_to_pledge.FesAppendixPanel', (function() {
    
    var Settings = instr_to_pledge.Settings,
        Button = qx.ui.form.Button,
        Label = qx.ui.basic.Label,
        FesGroupControl = instr_to_pledge.FesGroupControl;
    
    return {
        extend : qx.ui.container.Composite,
        
        construct : function() {
            this.base(arguments);
            
            this.initComponents();
        },
        
        members : {
            initComponents : function() {
                this.layout = new qx.ui.layout.Grid(Settings.GAP, Settings.GAP);
                this.layout.setColumnFlex(1, 1);
                
                this.setLayout(this.layout);
                
                var label = new FesGroupControl('Приложения');
                label.setAllowGrowX(true);
                
                this.add(label, {row: 0, column: 0, colSpan: 3});
                
                this.appendixTable = new qx.ui.table.Table(this.createModel());
                this.appendixTable.setHeight(70);
                this.appendixTable.setShowCellFocusIndicator(false);
                this.appendixTable.setStatusBarVisible(false);
                this.appendixTable.setColumnVisibilityButtonVisible(false);
                this.appendixTable.setHeaderCellsVisible(false);
                this.appendixTable.setColumnWidth(2, 230);
                this.add(this.appendixTable, {row: 1, column: 1});
                
                var buttonPanel = new qx.ui.container.Composite();
                buttonPanel.setLayout(new qx.ui.layout.Grid(Settings.GAP, Settings.GAP));
                
                this.addButton = new Button('...');
                buttonPanel.add(this.addButton, {row: 0, column: 0});
                this.editButton = new Button('...');
                buttonPanel.add(this.editButton, {row: 0, column: 1});
                this.delButton = new Button('...');
                buttonPanel.add(this.delButton, {row: 1, column: 0});
                this.scanButton = new Button('...');
                buttonPanel.add(this.scanButton, {row: 1, column: 1});
                buttonPanel.setAlignY('middle');
                buttonPanel.setAllowGrowY(false);
                this.add(buttonPanel, {row: 1, column: 2});
            },
            
            createModel : function() {
                this.appendixModel = new qx.ui.table.model.Simple();
                this.appendixModel.setColumns(['Номер', 'Дата', 'Наименование', 'Скан']);
                this.appendixModel.setData([['','','','']]);
                
                return this.appendixModel;
            },
            
            getLabelSize: function() {
                return 0;
            },

            setLabelSize: function(size) {
                this.getLayout().setColumnWidth(0, size);
            }
        }
    };
    
})());