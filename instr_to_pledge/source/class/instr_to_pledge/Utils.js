qx.Class.define('instr_to_pledge.Utils', (function() {

    return {
        statics: {
            getMaxWidth: function( /*controls*/ ) {
                if (!arguments[0]) {
                    return 0;
                }

                var arr;

                if (qx.lang.Type.isArray(arguments[0])) {
                    arr = arguments[0];
                }
                else {
                    arr = arguments;
                }

                var maxWidth = 0;

                for (var idx = 0; idx < arr.length; idx++) {
                    var width = qx.lang.Type.isNumber(arr[idx]) ? arr[idx] : arr[idx].getBounds().width;
                    if (width > maxWidth) {
                        maxWidth = width;
                    }
                }

                return maxWidth;
            }
        }
    };
})());