/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

/* ************************************************************************

#asset(instr_to_pledge/*)

************************************************************************ */

/**
 * This is the main application class of your custom application "instr_to_pledge"
 */
qx.Class.define("instr_to_pledge.Application", (function() {
    
    // Imports
    var FesInstructionToPledge = instr_to_pledge.FesInstructionToPledge;
    
    return {
        extend: qx.application.Standalone,



        /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

        members: {
            /**
             * This method contains the initial application code and gets called 
             * during startup of the application
             * 
             * @lint ignoreDeprecated(alert)
             */
            main: function() {
                // Call super class
                this.base(arguments);

                // Enable logging in debug variant
                if (qx.core.Environment.get("qx.debug")) {
                    // support native logging capabilities, e.g. Firebug for Firefox
                    qx.log.appender.Native;
                    // support additional cross-browser console. Press F7 to toggle visibility
                    qx.log.appender.Console;
                }

                /*
      -------------------------------------------------------------------------
        Below is your actual application code...
      -------------------------------------------------------------------------
      */

                // Create a button
                var button1 = new qx.ui.form.Button("First Button", "instr_to_pledge/test.png");

                // Document is the application root
                var doc = this.getRoot();

                // Add button to document at fixed coordinates
                doc.add(button1, {
                    left: 100,
                    top: 50
                });

                // Add an event listener
                button1.addListener("execute", function(e) {
                    this.createInstructionToPledge().open();
                }, this);
            },

            createInstructionToPledge: function() {
                var win = new FesInstructionToPledge();
                win.setModal(true);
                win.moveTo(150, 10);
                this.getRoot().add(win);

                return win;
            }
        }
    };
})());
