qx.Class.define('instr_to_pledge.FesInstructionToPledge', (function() {
    // Imports
    var Settings = instr_to_pledge.Settings,
        Window = qx.ui.window.Window,
        Grow = qx.ui.layout.Grow,
        Scroll = qx.ui.container.Scroll,
        FesInstructionToPledgePanel = instr_to_pledge.FesInstructionToPledgePanel;

    return {
        extend: Window,

        construct: function() {
            this.base(arguments, "Залоговое распоряжение");
            this.setWidth(740);
            this.setHeight(600);

            this.initComponents();
        },

        members: {
            initComponents: function() {
                this.setContentPadding(Settings.WINDOW_PADDING);
                var layout = new Grow();
                this.setLayout(layout);
                
                this.panel = new FesInstructionToPledgePanel();
                this.scroll = new Scroll(this.panel);

                this.add(this.scroll);
                
                this.addListenerOnce('appear', this.panel.fixLabelSizes, this.panel);
            }
        }
    };
})());
