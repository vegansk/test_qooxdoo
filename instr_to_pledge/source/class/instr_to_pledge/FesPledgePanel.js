qx.Class.define('instr_to_pledge.FesPledgePanel', (function() {
    
    var Settings = instr_to_pledge.Settings,
        Button = qx.ui.form.Button,
        Label = qx.ui.basic.Label,
        TextField = qx.ui.form.TextField,
        DateField = qx.ui.form.DateField,
        RadioButton = qx.ui.form.RadioButton,
        RadioGroup = qx.ui.form.RadioGroup,
        SelectBox = qx.ui.form.SelectBox,
        Utils = instr_to_pledge.Utils,
        CheckBox = qx.ui.form.CheckBox,
        TextArea = qx.ui.form.TextArea;
    
    return {
        extend: qx.ui.container.Composite,

        construct: function() {
            this.base(arguments);

            this.initComponents();
        },
        
        members: {
            initComponents: function() {
                this.setLayout(new qx.ui.layout.Grid(Settings.GAP, Settings.GAP));
                this.getLayout().setColumnAlign(0, 'right', 'middle');
                this.getLayout().setRowAlign(1, 'right', 'middle');
                this.getLayout().setColumnFlex(1, 1);
                
                var label = new Label('Вид залога:');
                this.add(label, {row: 0, column: 0});
                
                this.pledgeKindSelect = new SelectBox();
                this.setupPledgeKinds();
                this.add(this.pledgeKindSelect, {row: 0, column: 1});
                
                this.repeatCheck = new CheckBox('Повторно');
                this.add(this.repeatCheck, {row: 0, column: 2});
                
                this.innerPanel = new qx.ui.container.Composite();
                this.innerPanel.setLayout(new qx.ui.layout.Grid(Settings.GAP, Settings.GAP));
                this.innerPanel.getLayout().setRowAlign(0, 'right', 'middle');
                this.innerPanel.getLayout().setColumnFlex(1, 1);
                
                this.idLabel = new Label('Идентификатор ЦБ:');
                this.innerPanel.add(this.idLabel, {row: 0, column: 0});
                
                this.id = new TextArea();
                this.id.setMinimalLineHeight(2);
                this.innerPanel.add(this.id, {row: 0, column: 1});
                
                this.findButton = new Button('...');
                this.findButton.setAllowGrowY(false);
                this.innerPanel.add(this.findButton, {row: 0, column: 2});
                
                this.add(this.innerPanel, {row: 1, column: 0, colSpan: 3});
            },
            
            setupPledgeKinds: function() {
                this.pledgeKindSelect.add(new qx.ui.form.ListItem('Залог ЦБ'));
            },
            
            getLabelSize: function() {
                return Utils.getMaxWidth(this.idLabel);
            },

            setLabelSize: function(size) {
                this.innerPanel.getLayout().setColumnWidth(0, size);
                this.getLayout().setColumnWidth(0, size);
            }
        }
    };
    
})());