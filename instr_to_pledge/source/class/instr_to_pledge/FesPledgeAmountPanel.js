qx.Class.define('instr_to_pledge.FesPledgeAmountPanel', (function() {
    
    var Settings = instr_to_pledge.Settings,
        Label = qx.ui.basic.Label,
        TextField = qx.ui.form.TextField;
    
    return {
        extend: qx.ui.container.Composite,

        construct: function() {
            this.base(arguments);

            this.initComponents();
        },
        
        members: {
            initComponents: function() {
                this.setLayout(new qx.ui.layout.Grid(Settings.GAP, Settings.GAP));
                this.getLayout().setRowAlign(0, 'right', 'middle');
                this.getLayout().setColumnFlex(1, 1);
                this.getLayout().setColumnFlex(3, 1);
                
                this.amountLabel = new Label().set({
                    rich: true,
                    value: '<p style="text-align: right; margin: 0; padding: 0">Количество<br>закладываемых ЦБ:</p>'
                });
                this.add(this.amountLabel, {row: 0, column: 0});
                
                this.amountEdit = new TextField();
                this.add(this.amountEdit, {row: 0, column: 1});
                
                var label = new Label('Договор залога:');
                this.add(label, {row: 0, column: 2});
                
                this.contractNumEdit = new TextField();
                this.add(this.contractNumEdit, {row: 0, column: 3});
            },
            
            getLabelSize: function() {
                return this.amountLabel.getBounds().width;
            },

            setLabelSize: function(size) {
                this.getLayout().setColumnWidth(0, size);
            }
        }
    };
    
})());