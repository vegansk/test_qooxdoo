qx.Class.define('instr_to_pledge.FesLegalInfoPanel', (function() {
    
    var Settings = instr_to_pledge.Settings,
        Button = qx.ui.form.Button,
        Label = qx.ui.basic.Label,
        TextField = qx.ui.form.TextField,
        DateField = qx.ui.form.DateField,
        RadioButton = qx.ui.form.RadioButton,
        RadioGroup = qx.ui.form.RadioGroup,
        SelectBox = qx.ui.form.SelectBox,
        Utils = instr_to_pledge.Utils;
    
    return {
        extend: qx.ui.container.Composite,
        
        construct: function() {
            this.base(arguments);
            
            this.initComponents();
        },
        
        members: {
            initComponents: function() {
                this.setLayout(new qx.ui.layout.Grid(Settings.GAP, Settings.GAP));
                this.getLayout().setColumnAlign(0, 'right', 'middle');
                this.getLayout().setColumnAlign(4, 'right', 'middle');
                this.getLayout().setColumnFlex(3, 1);
                
                var nameLabel = new Label('Наименование:');
                this.add(nameLabel, {row: 0, column: 0});
                
                this.nameEdit = new TextField();
                this.add(this.nameEdit, {row: 0, column: 1, colSpan: 5});
                
                var docLabel = new Label('Документ:');
                this.add(docLabel, {row: 1, column: 0});
                
                this.ogrnRadio = new RadioButton('ОГРН');
                this.add(this.ogrnRadio, {row: 1, column: 1});
                
                this.otherDocRadio = new RadioButton('Иное');
                this.add(this.otherDocRadio, {row: 1, column: 2});
                
                this.docTypeGroup = new RadioGroup(this.ogrnRadio, this.otherDocRadio);
                
                this.docTypeSelect = new SelectBox();
                this.setupDocTypes();
                this.add(this.docTypeSelect, {row: 1, column: 3});
                
                var docNumLabel = new Label('№');
                this.add(docNumLabel, {row: 1, column: 4});
                
                this.docNumEdit = new TextField();
                this.add(this.docNumEdit, {row: 1, column: 5});
                
                var docPlaceLabel = new Label('Выдан:');
                this.add(docPlaceLabel, {row: 2, column: 0});
                
                this.docDateEdit = new DateField();
                this.docDateEdit.setAllowGrowY(false);
                this.docDateEdit.setAlignY('middle');
                this.add(this.docDateEdit, {row: 2, column: 1, colSpan: 2});
                
                var panel = new qx.ui.container.Composite();
                panel.setLayout(new qx.ui.layout.Grid(Settings.GAP, Settings.GAP));
                panel.getLayout().setColumnFlex(0, 1);
                panel.getLayout().setRowAlign(0, 'center', 'middle');

                this.docPlaceEdit = new TextField();
                this.docPlaceEdit.setAllowGrowY(false);
                panel.add(this.docPlaceEdit, {row: 0, column: 0});
                
                this.viewButton = new Button('...');
                panel.add(this.viewButton, {row: 0, column: 1});
                
                this.clearButton = new Button('...');
                panel.add(this.clearButton, {row: 0, column: 2});
                
                this.findButton = new Button('...');
                panel.add(this.findButton, {row: 0, column: 3});
                
                this.add(panel, {row: 2, column: 3, colSpan: 3});
            },
            
            setupDocTypes: function() {
                this.docTypeSelect.add(new qx.ui.form.ListItem(''));
            },
            
            setLabelSize: function(size) {
                this.getLayout().setColumnWidth(0, size);
            }
        }
    };
    
})());