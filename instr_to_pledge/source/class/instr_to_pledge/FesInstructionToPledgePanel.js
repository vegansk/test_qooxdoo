qx.Class.define('instr_to_pledge.FesInstructionToPledgePanel', (function() {
    // Imports
    var Settings = instr_to_pledge.Settings,
        VBox = qx.ui.layout.VBox,
        FesEmitentPanel = instr_to_pledge.FesEmitentPanel,
        FesDocNumPanel = instr_to_pledge.FesDocNumPanel,
        FesAppendixPanel = instr_to_pledge.FesAppendixPanel,
        FesGroupControl = instr_to_pledge.FesGroupControl,
        FesAccountIdentifierPanel = instr_to_pledge.FesAccountIdentifierPanel,
        FesPledgePanel = instr_to_pledge.FesPledgePanel,
        FesPledgeAmountPanel = instr_to_pledge.FesPledgeAmountPanel,
        FesPledgeConditionPanel = instr_to_pledge.FesPledgeConditionPanel;

    return {
        extend: qx.ui.container.Composite,

        construct: function() {
            this.base(arguments);
            this.initComponents();
        },

        members: {
            initComponents: function() {
                var layout = new VBox(Settings.GAP);
                this.setLayout(layout);
                
                this.emitentPanel = new FesEmitentPanel();
                this.add(this.emitentPanel);
                
                this.docNumPanel = new FesDocNumPanel();
                this.add(this.docNumPanel);
                
                this.appendixPanel = new FesAppendixPanel();
                this.add(this.appendixPanel);
                
                var delim = new FesGroupControl();
                this.add(delim);
                
                this.pledgerPanel = new FesAccountIdentifierPanel('Счет залогодателя:');
                this.add(this.pledgerPanel);
                
                delim = new FesGroupControl();
                this.add(delim);
                
                this.pledgeePanel = new FesAccountIdentifierPanel('Счет залогодержателя:');
                this.add(this.pledgeePanel);
                
                delim = new FesGroupControl();
                this.add(delim);
                
                this.pledgeIdPanel = new FesPledgePanel();
                this.add(this.pledgeIdPanel);
                
                delim = new FesGroupControl();
                this.add(delim);
                
                this.amountPanel = new FesPledgeAmountPanel();
                this.add(this.amountPanel);
                
                delim = new FesGroupControl('Условия залога');
                this.add(delim);
                
                this.conditionPanel = new FesPledgeConditionPanel();
                this.add(this.conditionPanel);
                
                this.addListenerOnce('appear', this.fixLabelSizes, this);
            },
            
            fixLabelSizes : function() {
                var panels = [this.emitentPanel, this.docNumPanel, this.appendixPanel, this.pledgerPanel,
                    this.pledgeePanel, this.pledgeIdPanel, this.amountPanel];
                var idx,
                    maxSize = 0;
                for(idx = 0; idx < panels.length; idx++) {
                    var size = panels[idx].getLabelSize();
                    if(size > maxSize) {
                        maxSize = size;
                    }
                }
                
                for(idx = 0; idx < panels.length; idx++) {
                    panels[idx].setLabelSize(maxSize);
                }
            }
        }
    };
})());
