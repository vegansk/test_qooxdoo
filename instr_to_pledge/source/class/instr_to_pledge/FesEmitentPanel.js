qx.Class.define('instr_to_pledge.FesEmitentPanel', (function() {

    var Settings = instr_to_pledge.Settings,
        Label = qx.ui.basic.Label,
        SelectBox = qx.ui.form.SelectBox;

    return {
        extend: qx.ui.container.Composite,

        construct: function() {
            this.base(arguments);
            this.initComponents();
        },

        members: {
            initComponents: function() {
                var layout = new qx.ui.layout.Grid(Settings.GAP, Settings.GAP);
                this.setLayout(layout);

                layout.setColumnAlign(0, "right", "middle");
                layout.setColumnFlex(1, 1);

                this.lbl = new Label("Эмитент:");
                this.emitent = new SelectBox();
                
                this.emitent.add(new qx.ui.form.ListItem("ЗАО \"Элдис-Софт\""));

                this.add(this.lbl, {
                    row: 0,
                    column: 0
                });
                this.add(this.emitent, {
                    row: 0,
                    column: 1
                });
            },
            
            getLabelSize : function() {
                return this.lbl.getBounds().width;
            },
            
            setLabelSize : function(size) {
                this.getLayout().setColumnWidth(0, size);
            }
        }
    };
})());
