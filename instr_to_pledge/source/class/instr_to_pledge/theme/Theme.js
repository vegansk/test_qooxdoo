/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("instr_to_pledge.theme.Theme",
{
  meta :
  {
    color : instr_to_pledge.theme.Color,
    decoration : instr_to_pledge.theme.Decoration,
    font : instr_to_pledge.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : instr_to_pledge.theme.Appearance
  }
});