qx.Class.define('instr_to_pledge.FesAccountIdentifierPanel', (function() {
    
    var Settings = instr_to_pledge.Settings,
        Button = qx.ui.form.Button,
        Label = qx.ui.basic.Label,
        TextField = qx.ui.form.TextField,
        DateField = qx.ui.form.DateField,
        RadioButton = qx.ui.form.RadioButton,
        RadioGroup = qx.ui.form.RadioGroup,
        SelectBox = qx.ui.form.SelectBox,
        Utils = instr_to_pledge.Utils,
        FesLegalInfoPanel = instr_to_pledge.FesLegalInfoPanel;
    
    return {
        extend: qx.ui.container.Composite,
        
        construct: function(label) {
            this.base(arguments);
            this.initLabel(label || 'Счет:');
            
            this.initComponents();
        },
        
        properties: {
            label: { check: 'String', deferredInit: true }  
        },
        
        members: {
            initComponents: function() {
                this.setLayout(new qx.ui.layout.Grid(Settings.GAP, Settings.GAP));
                this.getLayout().setColumnFlex(3, 1);
                this.getLayout().setRowAlign(0, 'center', 'middle');
                this.getLayout().setColumnAlign(0, 'right', 'middle');
                //this.getLayout().setColumnAlign(2, 'right', 'middle');
                
                this.accountLabel = new Label().set({
                    value: '<span style="color: blue">' + this.getLabel() + '</span>',
                    rich: true
                });
                this.add(this.accountLabel, {row: 0, column: 0});
                
                this.accountEdit = new TextField();
                this.add(this.accountEdit, {row: 0, column: 1});
                
                var accountTypeLabel = new Label('Тип счета:');
                accountTypeLabel.setMarginLeft(20);
                this.add(accountTypeLabel, {row: 0, column: 2});
                
                this.accountTypeSelect = new SelectBox();
                this.setupAccountTypes();
                this.add(this.accountTypeSelect, {row: 0, column: 3});
                
                this.ulRadio = new RadioButton('ЮЛ');
                this.add(this.ulRadio, {row: 0, column: 4});
                
                this.flRadio = new RadioButton('ФЛ');
                this.add(this.flRadio, {row: 0, column: 5});
                
                this.osRadio = new RadioButton('ОС');
                this.add(this.osRadio, {row: 0, column: 6});
                
                this.persTypeGroup = new RadioGroup(this.ulRadio, this.flRadio, this.osRadio);
                
                this.legalInfoPanel = new FesLegalInfoPanel();
                this.add(this.legalInfoPanel, {row: 1, column: 0, colSpan: 7});
            },
            
            setupAccountTypes: function() {
                var accTypes = ['Эмиссионный счет', 'Лицевой счет', 'Счет владельца', 'Счет номинального держателя', 'Казначейский счет'];
                
                for(var idx = 0; idx < accTypes.length; idx++) {
                    this.accountTypeSelect.add(new qx.ui.form.ListItem(accTypes[idx]));
                }
            },
            
            getLabelSize: function() {
                return Utils.getMaxWidth(this.accountLabel);
            },

            setLabelSize: function(size) {
                this.legalInfoPanel.setLabelSize(size);
                this.getLayout().setColumnWidth(0, size);
            }
        }
    };
    
})());