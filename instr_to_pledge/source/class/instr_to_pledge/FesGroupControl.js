qx.Class.define('instr_to_pledge.FesGroupControl', (function() {

    var Settings = instr_to_pledge.Settings,
        Label = qx.ui.basic.Label;

    return {
        extend: qx.ui.container.Composite,

        construct: function(label, gap, segSize) {
            this.base(arguments);
            if(label) {
                this.initLabel(label);
            }
            this.initGap(gap || Settings.GAP);
            this.initIndent(segSize || 20);

            this.initControls();
        },

        properties: {
            label: { check: 'String', nullable: true, deferredInit: true},
            gap: { check: 'Number', deferredInit: true},
            indent: { check: 'Number', deferredInit: true}
        },

        members: {
            initControls: function() {
                this.setLayout(new qx.ui.layout.Grid(this.getGap(), this.getGap()));
                this.getLayout().setColumnWidth(0, this.getIndent());
                this.getLayout().setColumnFlex(2, 1);

                var fl = new Label().set({
                    rich: true,
                    value: '<hr style="height: 1px; border:0; color: black; background-color: black;">'
                });
                fl.setAllowGrowX(true);
                this.add(fl, {
                    row: 0,
                    column: 0,
                    colSpan: this.getLabel() ? 1 : 3
                });

                if (this.getLabel()) {
                    var txtl = new Label().set({
                        rich: true,
                        value: '<span style="color: blue">' + this.getLabel() + '</span>'
                    });
                    this.add(txtl, {
                        row: 0,
                        column: 1
                    });

                    var ll = new Label().set({
                        rich: true,
                        value: '<hr style="height: 1px; border:0; color: black; background-color: black;">'
                    });
                    ll.setAllowGrowX(true);
                    this.add(ll, {
                        row: 0,
                        column: 2
                    });
                }
            }
        }
    };

})());