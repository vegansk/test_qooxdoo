qx.Class.define('instr_to_pledge.Settings', (function() {
    return {
        statics: {
            GAP: 3, // Default 5 pixels
            MIN_GAP: 1, // Gap for complex layouts
            WINDOW_PADDING: 4 
        }
    };
})());
